package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    public static List<int[]> fourNumberSum(int[] array, int targetSum) {
        validateInput(array);

        List<int[]> result = new ArrayList<>();
        for (int first = 0; first < array.length - 3; first++) {
            for (int second = first + 1; second < array.length - 2; second++) {
                for (int third = second + 1; third < array.length - 1; third++) {
                    for (int fourth = third + 1; fourth < array.length; fourth++) {
                        if (array[first] + array[second] + array[third] + array[fourth] == targetSum) {
                            result.add(new int[]{array[first], array[second], array[third], array[fourth]});
                        }
                    }
                }
            }
        }

        return result;
    }

    private static void validateInput(int[] array) {
        Objects.requireNonNull(array, "Input cannot be null");

        if (array.length < 4) {
            throw new RuntimeException("Array length should be > 4.");
        }
    }
}
