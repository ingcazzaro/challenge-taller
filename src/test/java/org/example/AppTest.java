package org.example;


import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test(expected = RuntimeException.class)
    public void validateInput() {
        App.fourNumberSum(new int[]{7, 6, 4}, 16);
        App.fourNumberSum(new int[]{}, 16);
        App.fourNumberSum(null, 16);
    }

    @Test
    public void exampleCases() {
        List<int[]> fourNumberSum1 = App.fourNumberSum(new int[]{7, 6, 4, -1, 1, 2}, 16);
        assertEquals(2, fourNumberSum1.size());
        assertArrayEquals(new int[]{7, 6, 4, -1}, fourNumberSum1.get(0));
        assertArrayEquals(new int[]{7, 6, 1, 2}, fourNumberSum1.get(1));

        List<int[]> fourNumberSum2 = App.fourNumberSum(new int[]{1, 2, 3, 4, 5, 6, 7}, 10);
        assertEquals(1, fourNumberSum2.size());
        assertArrayEquals(new int[]{1, 2, 3, 4}, fourNumberSum2.get(0));

        List<int[]> fourNumberSum3 = App.fourNumberSum(new int[]{5, -5, -2, 2, 3, -3}, 0);
        assertEquals(3, fourNumberSum3.size());
        assertArrayEquals(new int[]{5, -5, -2, 2}, fourNumberSum3.get(0));
        assertArrayEquals(new int[]{5, -5, 3, -3}, fourNumberSum3.get(1));
        assertArrayEquals(new int[]{-2, 2, 3, -3}, fourNumberSum3.get(2));

        List<int[]> fourNumberSum4 = App.fourNumberSum(new int[]{-2, -1, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 4);
        assertEquals(4, fourNumberSum4.size());
        assertArrayEquals(new int[]{-2, -1, 1, 6}, fourNumberSum4.get(0));
        assertArrayEquals(new int[]{-2, -1, 2, 5}, fourNumberSum4.get(1));
        assertArrayEquals(new int[]{-2, -1, 3, 4}, fourNumberSum4.get(2));
        assertArrayEquals(new int[]{-2, 1, 2, 3}, fourNumberSum4.get(3));

        List<int[]> fourNumberSum5 = App.fourNumberSum(new int[]{-1, 22, 18, 4, 7, 11, 2, -5, -3}, 30);
        assertEquals(5, fourNumberSum5.size());
        assertArrayEquals(new int[]{-1, 22, 7, 2}, fourNumberSum5.get(0));
        assertArrayEquals(new int[]{22, 4, 7, -3}, fourNumberSum5.get(2));
        assertArrayEquals(new int[]{-1, 18, 11, 2}, fourNumberSum5.get(1));
        assertArrayEquals(new int[]{18, 4, 11, -3}, fourNumberSum5.get(4));
        assertArrayEquals(new int[]{22, 11, 2, -5}, fourNumberSum5.get(3));

        List<int[]> fourNumberSum6 = App.fourNumberSum(new int[]{-10, -3, -5, 2, 15, -7, 28, -6, 12, 8, 11, 5}, 20);
        assertEquals(7, fourNumberSum6.size());
        assertArrayEquals(new int[]{-10, -3, 28, 5}, fourNumberSum6.get(0));
        assertArrayEquals(new int[]{-10, 28, -6, 8}, fourNumberSum6.get(1));
        assertArrayEquals(new int[]{-3, 2, -7, 28}, fourNumberSum6.get(2));
        assertArrayEquals(new int[]{-5, 2, 15, 8}, fourNumberSum6.get(3));
        assertArrayEquals(new int[]{-5, 2, 12, 11}, fourNumberSum6.get(4));
        assertArrayEquals(new int[]{-5, 12, 8, 5}, fourNumberSum6.get(5));
        assertArrayEquals(new int[]{-7, 28, -6, 5}, fourNumberSum6.get(6));

        List<int[]> fourNumberSum7 = App.fourNumberSum(new int[]{1, 2, 3, 4, 5}, 100);
        assertEquals(0, fourNumberSum7.size());

        List<int[]> fourNumberSum8 = App.fourNumberSum(new int[]{1, 2, 3, 4, 5, -5, 6, -6}, 5);
        assertEquals(6, fourNumberSum8.size());
        assertArrayEquals(new int[]{1, 3, -5, 6}, fourNumberSum8.get(0));
        assertArrayEquals(new int[]{1, 4, 5, -5}, fourNumberSum8.get(1));
        assertArrayEquals(new int[]{1, 4, 6, -6}, fourNumberSum8.get(2));
        assertArrayEquals(new int[]{2, 3, 5, -5}, fourNumberSum8.get(3));
        assertArrayEquals(new int[]{2, 3, 6, -6}, fourNumberSum8.get(4));
        assertArrayEquals(new int[]{2, 4, 5, -6}, fourNumberSum8.get(5));

    }
}
